
import fs from 'fs'

const BASE_URL = 'http://localhost:3000'
const res = await fetch(`${BASE_URL}/?limit=137`)
const data = await res.json()
const pals = data.content
const new_pals = pals.map(p => {
    return {
        id: p.id,
        number: p.key,
        name: p.name,
        types: p.types,
        suitability: p.suitability,
        drops: p.drops
    }
})

// CSV
fs.unlinkSync('helpers/pals.csv')
const columns = `ID, Number, Name, Types, Work Traits, Drop, Caputured, 10 Captured\n`;
fs.appendFileSync('helpers/pals.csv', columns);
new_pals.forEach(p => {
    const types = p.types.map(t =>  t.name).join(' ')
    const suitability = p.suitability.map( s => `${s.type} ${s.level}`).join(' ')
    const drops = p.drops.join(' ')
    const row = `${p.id}, ${p.number}, ${p.name}, ${types}, ${suitability}, ${drops}, false, false\n`
    fs.appendFileSync('helpers/pals.csv', row);
});

// JSON
const json_data = JSON.stringify(new_pals)
fs.writeFile('helpers/pals.json', json_data, (err) => {console.log(err ?? 'ok')});

const json_data_full = JSON.stringify(pals)
fs.writeFile('helpers/pals_full.json', json_data_full, (err) => {console.log(err ?? 'ok')});