import axios from 'axios'
import fs from 'fs'

async function downloadImage(url, filename) {
    const response = await axios.get(url, { responseType: 'arraybuffer' });

    fs.writeFile(filename, response.data, (err) => {
        if (err) throw err;
        console.log(`Image ${filename} ok!`);
    });
  }

const download = async (url, folder, filename) => {
    console.log('Connecting …')
    const { data, headers } = await axios({
      url,
      method: 'GET',
      responseType: 'stream',
    })
    const totalLength = headers['content-length']
  
    console.log('Starting download')
    const progressBar = new ProgressBar('-> downloading [:bar] :percent :etas', {
      width: 40,
      complete: '=',
      incomplete: ' ',
      renderThrottle: 1,
      total: parseInt(totalLength),
    })
  
    const writer = fs.createWriteStream(path.resolve(import.meta.dir, folder, filename))
  
    data.on('data', (chunk) => progressBar.tick(chunk.length))
    data.pipe(writer)
  }


const palsDir = 'public/images/paldeck'
const typesDir = 'public/images/elements'
const workDir = 'public/images/works'

const BASE_URL = 'http://localhost:3000'
const res = await fetch(`${BASE_URL}/?limit=137`)
const data = await res.json()
const pals = data.content

let typesMap = new Map()
let workMap = new Map()

let palRoutes = []
let typesRoutes = []
let workRoutes = []

pals.map(p => {

    palRoutes.push(BASE_URL+p.image)

    p.types.map(t => {
        if(!typesMap[t.name]){
            typesMap[t.name] = 1
            typesRoutes.push(BASE_URL+t.image)
        }
    })

    p.suitability.map(s => {
        if(!workMap[s.type]){
            workMap[s.type] = 1
            workRoutes.push(BASE_URL+s.image)
        }
    })

})

console.log('Starting pals')
for(const item of palRoutes){
    const itemName = palsDir + '/' + item.split('/').slice(-1)
    await downloadImage(item, itemName)
}
console.log('Finished pals')
console.log('==================================')

console.log('Starting types')
for(const item of typesRoutes){
    const itemName = typesDir + '/' + item.split('/').slice(-1)
    await downloadImage(item, itemName)
}
console.log('Finished types ')
console.log('==================================')

console.log('Starting work')
for(const item of workRoutes){
    const itemName = workDir + '/' + item.split('/').slice(-1)
    await downloadImage(item, itemName)
}
console.log('Finished work')
console.log('==================================')