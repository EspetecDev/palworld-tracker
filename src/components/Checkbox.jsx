import { useEffect, useState } from "react"



export default function Checkbox({id, isTen}){

    const [checked, setIsChecked] = useState(false);

    useEffect( () => {
        const state = localStorage.getItem(`${id}-${isTen ? '10capture':'capture'}`) ?? false;
        setIsChecked(state);
    }, [])

    const handleState = (e) => {
        setIsChecked(e.target.checked)
        localStorage.setItem(`${id}-${isTen ? '10capture':'capture'}`, e.target.checked);
    }

    return(
        <div className="form-control">
            <label className="cursor-pointer label">
                <input id={id} onChange={handleState} type="checkbox" defaultChecked className="checkbox checkbox-warning" checked={checked}/>
            </label>
        </div>
    )
}