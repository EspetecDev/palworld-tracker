import Checkbox from "./Checkbox";
import PalWorkPerk from "./PalWorkPerk"


export default function PalTable({pals}) {

    // const BASE_URL = 'http://localhost:3000'
    const BASE_URL = ''

    function handleInput(id, isTen, e){
        const state = localStorage.getItem(`${id}-${isTen ? 'capture':'10capture'}`) ?? false;
        localStorage.setItem(`${id}-${isTen ? '10capture':'capture'}`, !state);
    }

    return(
        <div className="overflow-x-auto m-4 rounded-box">
            <table className="table">
                <thead >
                    <tr className=" bg-teal-800 text-gray-950">
                        <th>#</th>
                        <th>Picture</th>
                        <th>Name</th>
                        <th>Types</th>
                        <th>Work Perks</th>
                        <th>Drops</th>
                        <th>Captured</th>
                        <th>10 Captured</th>
                    </tr>
                </thead>
                <tbody>
                    {pals.map((p, idx) => (
                        <tr key={p.id} className=" bg-slate-800 ">
                            <td className="text-2xl">{p.key}</td>
                            <td>
                            <div className="avatar">
                                <div className="w-12 mask mask-squircle">
                                    <img src={BASE_URL+p.image} />
                                </div>
                            </div>
                            </td>
                            <td className="text-2xl">{p.name}</td>
                            <td>
                                {p.types.map(t =>(
                                    <div key={t.name} className="tooltip" data-tip={t.name.charAt(0).toUpperCase() + t.name.slice(1)}>
                                        <img src={BASE_URL+t.image} />
                                    </div>
                                ))}
                            </td>
                            <td>
                                {p.suitability.map(s => (
                                    <PalWorkPerk key={s.type} {...s} />
                                ))
                                }
                            </td>
                            <td>
                                {p.drops.map(d => (
                                    d
                                ))
                                }
                            </td>
                            <td>
                                <Checkbox id={p.id} isTen={false}/>
                            </td>
                            <td>
                                <Checkbox id={p.id} isTen={true}/>
                            </td>

                        </tr>
                     ))
                    }
                </tbody>
            </table>
        </div>
    )
}